/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: LicenseRevertTest.java
 * Date: 2020-04-14
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.server;

import java.io.IOException;
import java.text.ParseException;

/**
 * @author 三刀
 * @version V1.0 , 2020/4/14
 */
public class LicenseRevertTest {
    public static void main(String[] args) throws IOException, ParseException {
        String[] file = new String[]{"source.txt"};
        LicenseRevert.main(file);
    }
}
